package system_de_fichiers;

public class Main {

    public static void main(String[] str){
        Systeme s = new Systeme();

        Fichier f1 = new Fichier("f1", 20);
        Fichier f2 = new Fichier("f2", 25);
        Fichier f3 = new Fichier("f3", 50);
        Fichier f4 = new Fichier("f4", 60);
        Fichier f5 = new Fichier("f5", 30);
        Fichier f6 = new Fichier("f6", 40);

        Repertoire r1 = new Repertoire("r1");
        Repertoire r2 = new Repertoire("r2");
        Repertoire r3 = new Repertoire("r3");

        r1.addElement(f2);
        r1.addElement(r3);

        r2.addElement(f3);
        r2.addElement(f4);

        r3.addElement(f5);
        r3.addElement(f6);

        s.addElement(r1);
        s.addElement(r2);

        Repertoire s4 = new Repertoire("r4");
        Repertoire s5 = new Repertoire("r5");
        Repertoire s6 = new Repertoire("r6");

        r1.addElement(s4);
        s4.addElement(s5);
        s5.addElement(s6);

        s6.addElement(f1);

        System.out.println(r1.taille());
    }
}
