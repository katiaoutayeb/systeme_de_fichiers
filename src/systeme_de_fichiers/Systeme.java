package system_de_fichiers;

public class Fichier extends Element{
	private int taille;

	public Fichier(String name, int taille) {
		super(name);
		this.taille = taille;
	}

	@Override
	public int taille() {
		return taille;
	}

	@Override
	public boolean isChild(Element element) {
		return false;
	}
}
