package systeme_de_fichiers;

import java.util.ArrayList;

public class Repertoire extends Element{
	private ArrayList<Element> elements = new ArrayList<Element>();

public Repertoire(String name) {
        super(name);
        this.elements = new ArrayList<Element>();
    }

 public boolean addElement(Element e){
        if(!(e instanceof Fichier))
            if (!e.isChild(this))
                return false;
        elements.add(e);
        return true;
    }

 /**
     * recherche si element n'est pas un fils
     * @return boolean
     */
    public boolean isChild(Element element){
        if(this == element) return false;
        for (Element e: elements) {
            if(element.isChild(e)) return false;
        }
        return true;
    }

public boolean removeElement(Element e){
        elements.remove(e);
        return true;
    }

    @Override
    public int taille(){
        int taille = 0;
        for (Element e: elements) {
            taille += e.taille();
        }
        return taille;
    }

}
