package systeme_de_fichiers;

public abstract class Element {
	private String name;

	public Element(String name) {
        this.name = name;
   	}

	public String getName() {
        return name;
    	}

	public void setName(String name) {
        this.name = name;
    	}

	public abstract int taille();

    	public abstract boolean isChild(Element element);
}
